/**
 * A library who contains classes and tools that provides extra <code>numeric</code> methods and implementations - version: 1.0.0 - license: MPL 2.0/GPL 2.0+/LGPL 2.1+ - Follow me on Twitter! @ekameleon
 */

(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
  typeof define === 'function' && define.amd ? define(factory) :
  (global.vegas_numeric = factory());
}(this, (function () { 'use strict';

  function _defineProperty(obj, key, value) {
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }

    return obj;
  }

  function _objectSpread(target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i] != null ? arguments[i] : {};
      var ownKeys = Object.keys(source);

      if (typeof Object.getOwnPropertySymbols === 'function') {
        ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) {
          return Object.getOwnPropertyDescriptor(source, sym).enumerable;
        }));
      }

      ownKeys.forEach(function (key) {
        _defineProperty(target, key, source[key]);
      });
    }

    return target;
  }

  function PRNG() {
    var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;
    Object.defineProperties(this, {
      _value: {
        value: 1,
        writable: true
      }
    });
    this.value = value > 0 ? value : Math.random() * 0X7FFFFFFE;
  }
  PRNG.prototype = Object.create(Object.prototype, {
    constructor: {
      writable: true,
      value: PRNG
    },
    value: {
      get: function get() {
        return this._value;
      },
      set: function set(value) {
        value = value > 1 ? value : 1;
        value = value > 0X7FFFFFFE ? 0X7FFFFFFE : value;
        this._value = value;
      }
    },
    randomInt: {
      value: function value() {
        this._value = this._value * 16807 % 2147483647;
        return this._value;
      }
    },
    randomIntByMinMax: {
      value: function value() {
        var min = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
        var max = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;
        if (isNaN(min)) {
          min = 0;
        }
        if (isNaN(max)) {
          max = 1;
        }
        min -= 0.4999;
        max += 0.4999;
        this._value = this._value * 16807 % 2147483647;
        return Math.round(min + (max - min) * this._value / 2147483647);
      }
    },
    randomIntByRange: {
      value: function value(r) {
        var min = r.min - 0.4999;
        var max = r.max + 0.4999;
        this._value = this._value * 16807 % 2147483647;
        return Math.round(min + (max - min) * this._value / 2147483647);
      }
    },
    randomNumber: {
      value: function value()
      {
        this._value = this._value * 16807 % 2147483647;
        return this._value / 2147483647;
      }
    },
    randomNumberByMinMax: {
      value: function value(min, max) {
        if (isNaN(min)) {
          min = 0;
        }
        if (isNaN(max)) {
          max = 1;
        }
        this._value = this._value * 16807 % 2147483647;
        return min + (max - min) * this._value / 2147483647;
      }
    },
    randomNumberByRange: {
      value: function value(r
      )
      {
        this._value = this._value * 16807 % 2147483647;
        return r.min + (r.max - r.min) * this._value / 2147483647;
      }
    },
    toString: {
      value: function value() {
        return String(this._value);
      }
    },
    valueOf: {
      value: function value() {
        return this._value;
      }
    }
  });

  function Range() {
    var min = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : NaN;
    var max = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : NaN;
    var writable = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;
    if (max < min) {
      throw new RangeError("The Range constructor failed, the 'max' argument is < of 'min' argument");
    }
    Object.defineProperties(this, {
      max: {
        writable: writable,
        value: isNaN(max) ? NaN : max
      },
      min: {
        writable: writable,
        value: isNaN(min) ? NaN : min
      }
    });
  }
  Range.prototype = Object.create(Object.prototype, {
    constructor: {
      writable: true,
      value: Range
    },
    clamp: {
      value: function value(_value) {
        if (isNaN(_value)) {
          return NaN;
        }
        var mi = this.min;
        var ma = this.max;
        if (isNaN(mi)) {
          mi = _value;
        }
        if (isNaN(ma)) {
          ma = _value;
        }
        return Math.max(Math.min(_value, ma), mi);
      }
    },
    clone: {
      writable: true,
      value: function value() {
        return new Range(this.min, this.max);
      }
    },
    combine: {
      value: function value() {
        var range = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
        if (!(range instanceof Range)) {
          return this.clone();
        } else {
          return new Range(Math.min(this.min, range.min), Math.max(this.max, range.max));
        }
      }
    },
    contains: {
      value: function value(_value2) {
        return !(_value2 > this.max || _value2 < this.min);
      }
    },
    equals: {
      writable: true,
      value: function value(o) {
        if (o instanceof Range) {
          return o.min === this.min && o.max === this.max;
        } else {
          return false;
        }
      }
    },
    expand: {
      value: function value() {
        var lowerMargin = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
        var upperMargin = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
        if (isNaN(lowerMargin)) {
          lowerMargin = 1;
        }
        if (isNaN(upperMargin)) {
          upperMargin = 1;
        }
        var delta = this.max - this.min;
        return new Range(this.min - delta * lowerMargin, this.max + delta * upperMargin);
      }
    },
    getCentralValue: {
      value: function value() {
        return (this.min + this.max) / 2;
      }
    },
    getRandomFloat: {
      value: function value() {
        return Math.random() * (this.max - this.min) + this.min;
      }
    },
    getRandomInteger: {
      value: function value() {
        return Math.floor(Math.random() * (this.max - this.min) + this.min);
      }
    },
    isOutOfRange: {
      value: function value(_value3) {
        return _value3 > this.max || _value3 < this.min;
      }
    },
    overlap: {
      value: function value(range) {
        return this.max >= range.min && range.max >= this.min;
      }
    },
    size: {
      value: function value() {
        return this.max - this.min;
      }
    },
    toString: {
      writable: true,
      value: function value() {
        return "[Range min:" + this.min + " max:" + this.max + "]";
      }
    }
  });
  Object.defineProperties(Range, {
    COLOR: {
      value: new Range(-255, 255, false),
      enumerable: true
    },
    DEGREE: {
      value: new Range(0, 360, false),
      enumerable: true
    },
    PERCENT: {
      value: new Range(0, 100, false),
      enumerable: true
    },
    RADIAN: {
      value: new Range(0, Math.PI * 2, false),
      enumerable: true
    },
    UNITY: {
      value: new Range(0, 1, false),
      enumerable: true
    }
  });

  function RomanNumber() {
    var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
    Object.defineProperties(this, {
      _num: {
        value: 0,
        writable: true
      }
    });
    if (typeof value === "string" || value instanceof String) {
      this._num = RomanNumber.parseRomanString(value);
    } else if (typeof value === "number" || value instanceof Number) {
      if (value > RomanNumber.MAX) {
        throw new RangeError("Max value for a RomanNumber is " + RomanNumber.MAX);
      }
      if (value < RomanNumber.MIN) {
        throw new RangeError("Min value for a RomanNumber is " + RomanNumber.MIN);
      }
      this._num = value;
    }
  }
  Object.defineProperties(RomanNumber, {
    MAX: {
      value: 3999,
      enumerable: true
    },
    MIN: {
      value: 0,
      enumerable: true
    },
    NUMERIC: {
      value: [1000, 500, 100, 50, 10, 5, 1],
      enumerable: true
    },
    ROMAN: {
      value: ["M", "D", "C", "L", "X", "V", "I"],
      enumerable: true
    },
    parse: {
      value: function value(num) {
        var MAX = RomanNumber.MAX;
        var MIN = RomanNumber.MIN;
        var NUMERIC = RomanNumber.NUMERIC;
        var ROMAN = RomanNumber.ROMAN;
        var n = 0;
        var r = "";
        if (typeof num === "number" || num instanceof Number) {
          if (num > RomanNumber.MAX) {
            throw new RangeError("Max value for a RomanNumber is " + MAX);
          } else if (num < RomanNumber.MIN) {
            throw new RangeError("Min value for a RomanNumber is " + MIN);
          }
          n = num;
        }
        var i;
        var rank
        ;
        var bellow
        ;
        var roman;
        var romansub;
        var size = NUMERIC.length;
        for (i = 0; i < size; i++) {
          if (n === 0) {
            break;
          }
          rank = NUMERIC[i];
          roman = ROMAN[i];
          if (String(rank).charAt(0) === "5") {
            bellow = rank - NUMERIC[i + 1];
            romansub = ROMAN[i + 1];
          } else {
            bellow = rank - NUMERIC[i + 2];
            romansub = ROMAN[i + 2];
          }
          if (n >= rank || n >= bellow) {
            while (n >= rank) {
              r += roman;
              n -= rank;
            }
          }
          if (n > 0 && n >= bellow) {
            r += romansub + roman;
            n -= bellow;
          }
        }
        return r;
      }
    },
    parseRomanString: {
      value: function value(roman) {
        var NUMERIC = RomanNumber.NUMERIC;
        var ROMAN = RomanNumber.ROMAN;
        if (roman === null || roman === "") {
          return 0;
        }
        roman = roman.toUpperCase();
        var n = 0;
        var pos = 0;
        var ch = "";
        var next = "";
        var ich;
        var inext;
        while (pos >= 0) {
          ch = roman.charAt(pos);
          next = roman.charAt(pos + 1);
          if (ch === "") {
            break;
          }
          ich = ROMAN.indexOf(ch);
          inext = ROMAN.indexOf(next);
          if (ich < 0) {
            return 0;
          } else if (ich <= inext || inext === -1) {
            n += NUMERIC[ich];
          } else {
            n += NUMERIC[inext] - NUMERIC[ich];
            pos++;
          }
          pos++;
        }
        return n;
      }
    }
  });
  RomanNumber.prototype = Object.create(Object.prototype, {
    constructor: {
      writable: true,
      value: RomanNumber
    },
    parse: {
      value: function value(_value) {
        return RomanNumber.parse(typeof _value === "number" || _value instanceof Number ? _value : this._num);
      }
    },
    toString: {
      value: function value() {
        return this.parse(this._num);
      }
    },
    valueOf: {
      value: function value()
      {
        return this._num;
      }
    }
  });

  /**
   * The {@link system.numeric} library contains classes and tools that provides extra <code>numeric</code> methods and implementations.
   * @summary The {@link system.numeric} library contains classes and tools that provides extra <code>numeric</code> methods and implementations.
   * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
   * @author Marc Alcaraz <ekameleon@gmail.com>
   * @namespace system.numeric
   * @memberof system
   */
  var data = {
    PRNG: PRNG,
    Range: Range,
    RomanNumber: RomanNumber
  };

  var skip = false ;
  function sayHello( name = '' , version = '' , link = '' )
  {
      if( skip )
      {
          return ;
      }
      try
      {
          if ( navigator && navigator.userAgent && navigator.userAgent.toLowerCase().indexOf('chrome') > -1)
          {
              const args = [
                  `\n %c %c %c ${name} ${version} %c %c ${link} %c %c\n\n`,
                  'background: #ff0000; padding:5px 0;',
                  'background: #AA0000; padding:5px 0;',
                  'color: #F7FF3C; background: #000000; padding:5px 0;',
                  'background: #AA0000; padding:5px 0;',
                  'color: #F7FF3C; background: #ff0000; padding:5px 0;',
                  'background: #AA0000; padding:5px 0;',
                  'background: #ff0000; padding:5px 0;',
              ];
              window.console.log.apply( console , args );
          }
          else if (window.console)
          {
              window.console.log(`${name} ${version} - ${link}`);
          }
      }
      catch( error )
      {
      }
  }
  function skipHello()
  {
      skip = true ;
  }

  var metas = Object.defineProperties({}, {
    name: {
      enumerable: true,
      value: 'vegas-js-numeric'
    },
    description: {
      enumerable: true,
      value: 'A library who contains classes and tools that provides extra <code>numeric</code> methods and implementations'
    },
    version: {
      enumerable: true,
      value: '1.0.0'
    },
    license: {
      enumerable: true,
      value: "MPL-2.0 OR GPL-2.0+ OR LGPL-2.1+"
    },
    url: {
      enumerable: true,
      value: 'https://bitbucket.org/ekameleon/vegas-js-numeric'
    }
  });
  var bundle = _objectSpread({
    metas: metas,
    sayHello: sayHello,
    skipHello: skipHello
  }, data);
  try {
    if (window) {
      window.addEventListener('load', function load() {
        window.removeEventListener("load", load, false);
        sayHello(metas.name, metas.version, metas.url);
      }, false);
    }
  } catch (ignored) {}

  return bundle;

})));
//# sourceMappingURL=vegas.numeric.js.map
